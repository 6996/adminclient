/**
 * Created by HOME on 1/5/2017.
 */
(function () {
    'use strict';

    angular
        .module('CheapAdmin', ['ui.router', 'ngMessages', 'ngStorage', 'ngMaterial','ngAnimate','ngSanitize'])
        .config(config)
        .run(run);

    function config($stateProvider, $urlRouterProvider, $locationProvider) {
        // default route
        $urlRouterProvider.otherwise("/");

        // app routes
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'admin_page.html',
                controller: 'AdminCtrl',
                controllerAs: 'vn'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'admin_login.html',
                controller: 'LoginCtrl',
                controllerAs: 'vm'
            })
            .state('admin_chuyenbay', {
                url: '/admin_chuyenbay',
                templateUrl: 'admin_chuyenbay.html',
                controller:'FlightCtrl'
            })
            .state('admin_sanbay', {
                url: '/admin_sanbay',
                templateUrl: 'admin_sanbay.html',
                controller:'SanbayCtrl'
            })
            .state('admin_passenger', {
                url: '/admin_hanhkhach',
                templateUrl: 'admin_hanhkhach.html',
                controller:'PassengerCtrl'
            })
            .state('search_flightregis', {
                url: '/search_flightregis',
                templateUrl: 'admin_timkiem.html',
                controller:'validateCtrl'
            });
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');
    }

    function run($rootScope, $http, $location, $localStorage) {
        // keep user logged in after page refresh
        if ($localStorage.currentUser) {
            $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.currentUser.token;
        }

        // redirect to login page if not logged in and trying to access a restricted page
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            var publicPages = ['/login'];
            var restrictedPage = publicPages.indexOf($location.path()) === -1;
            if (restrictedPage && !$localStorage.currentUser) {
                $location.path('/login');
            }
        });
    }
})();