/**
 * Created by HOME on 1/5/2017.
 */
(function () {
    'use strict';

    angular
        .module('CheapAdmin')
        .controller('LoginCtrl', Controller);

    function Controller($location, AuthenticationService) {
        var vm = this;

        vm.login = login;

        initController();

        function initController() {
            // reset login status
            AuthenticationService.Logout();
        }

        function login() {
            vm.loading = true;
            AuthenticationService.Login(vm.username, vm.password, function (result) {
                if (result === true) {
                    $location.path('/');
                } else {

                }
            });
        }
    }
})();