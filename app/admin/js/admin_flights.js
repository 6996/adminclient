var app = angular.module('CheapAdmin');
app.controller('FlightCtrl', ['$scope','$http', function($scope,$http) {
    $scope.listicket = [];


    $scope.editData = {};

    function getFlight () {
        $http.get('http://localhost:3000/api/admin/chuyenbay').success(function(response){
            $scope.flights = response;

            for (var i = 0 ; i < response.length; i++) {
                $scope.editData[i] = false;
            }
        });
    }
    getFlight();


    $scope.Edit = function(index){
        $scope.editData[index.edit] = true;
        if ($('#sidebar > ul').is(":visible") === true) {
            $('#main-content').css({
                'margin-left': '0px'
            });
            $('#sidebar').css({
                'margin-left': '-210px'
            });
            $('#sidebar > ul').hide();
            $("#container").addClass("sidebar-closed");
        }
    };

    $scope.Save = function(index, item){
        $scope.editData[index.edit] = false;

        var data = {
            Ma:  item.Ma,
            SanBayDi: item.SanBayDi,
            SanBayDen: item.SanBayDen,
            ThoiGianDi: item.ThoiGianDi,
            Hang: item.Hang,
            MucGia: item.MucGia,
            SoLuongGhe: item.SoLuongGhe,
            GiaBan: item.GiaBan,
            GheDaDat: item.GheDaDat
        };
        $http.put('http://localhost:3000/api/admin/chuyenbay/' + $scope.flights[index]._id, data).success(function(response){
            getFlight();

        });
        if ($('#sidebar > ul').is(":visible") != true) {
            $('#main-content').css({
                'margin-left': '210px'
            });
            $('#sidebar > ul').show();
            $('#sidebar').css({
                'margin-left': '0'
            });
            $("#container").removeClass("sidebar-closed");
        }
    };

    $scope.AddFlight = function () {
        var data = {
            Ma:  $scope.addma,
            SanBayDi: $scope.addnoidi,
            SanBayDen: $scope.addnoiden,
            ThoiGianDi: $scope.addngaydi,
            Hang: $scope.addhang,
            MucGia: $scope.addmucgia,
            SoLuongGhe: $scope.addsoghe,
            GiaBan: $scope.addgiaban,
            GheDaDat: $scope.addghedadat
        };
        $http.post('http://localhost:3000/api/admin/chuyenbay', data).success(function(response){
            getFlight();
        });
            $scope.addma = null;
            $scope.addnoidi = null;
            $scope.addnoiden = null;
            $scope.addngaydi = null;
            $scope.addhang = null;
            $scope.addmucgia = null;
            $scope.addsoghe = null;
            $scope.addgiaban = null;
            $scope.addghedadat = null;
    };

    $scope.DeleteFlight = function (item) {
        $http.delete('http://localhost:3000/api/admin/chuyenbay/' + item._id).success(function(response){
            getFlight();
        });
    };


}]);