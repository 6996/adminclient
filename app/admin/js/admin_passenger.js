var app = angular.module('CheapAdmin');
app.controller('PassengerCtrl', ['$scope','$http', function($scope,$http) {
    $scope.listicket = [];


    $scope.editData = {};
    $scope.passengers=[];

    function Search() {
        $http.get('http://localhost:3000/api/admin/hanhkhach/' + $scope.txtMaCBay).success(function(response){

            for(var i=0;i < response.length;i++) {
                if (response[i].DanhSach[0]) {
                    for (var j = 0; j < response[i].DanhSach[0].HanhKhach.length; j++) {
                        var jsonObj = response[i].DanhSach[0].HanhKhach[j];
                        jsonObj["MaDatCho"] = response[i].DanhSach[0].MaDatCho;
                        $scope.passengers.push(jsonObj);
                        if (response[i].DanhSach[0].HanhKhach[j] == null) {
                            console.log("Cấu trúc dữ liệu lỗi");
                        }
                    }
                }
            }
            for (var k = 0 ; k < $scope.passengers.length; k++) {
                $scope.editData[k] = false;
            }
        });
    }

    $scope.searchFromMaDatCho = function () {
        Search();
    };
    $scope.Edit = function(index, item){
        $scope.editData[index.edit] = true;
        item["DanhXungCu"] = item.DanhXung;
        item["HoCu"]=item.Ho;
        item["TenCu"]=item.Ten;
        console.log(item);

    };

    $scope.Save = function(index, item){
        var jsonObj1 = {};
        jsonObj1["MaDatCho"] = item.MaDatCho;
        console.log(item.TenCu);

        jsonObj1["ThongTinCu"] = {
            DanhXung : item.DanhXungCu,
            Ho : item.HoCu,
            Ten: item.TenCu
        };
        jsonObj1["ThongTinMoi"] = {
            DanhXung: item.DanhXung,
            Ho: item.Ho,
            Ten: item.Ten
        };
        console.log(jsonObj1);
        $scope.editData[index.edit] = false;
        $http.put('http://localhost:3000/api/admin/hanhkhach/', jsonObj1).success(function(response){
            Search();
        });
    };
}]);