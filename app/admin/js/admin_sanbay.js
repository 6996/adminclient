var app = angular.module('CheapAdmin');
app.controller('SanbayCtrl', ['$scope','$http', function($scope , $http) {
    $scope.listicket = [];

    $scope.editData = {};

    function getAirPort () {
        $http.get('http://localhost:3000/api/chuyenbay/sanbaydi').success(function(response){
            $scope.airports = response;

            for (var i = 0 ; i < response.length; i++) {
                $scope.editData[i] = false;
            }
        });
    }
    getAirPort();

    $scope.Edit = function(index){
        $scope.editData[index.edit] = true;
        if ($('#sidebar > ul').is(":visible") === true) {
            $('#main-content').css({
                'margin-left': '0px'
            });
            $('#sidebar').css({
                'margin-left': '-210px'
            });
            $('#sidebar > ul').hide();
            $("#container").addClass("sidebar-closed");
        }
    };

    $scope.Save = function(index, item){
        $scope.editData[index.edit] = false;

        var data = {
            Ten: item.Ten,
            MaSanBay: item.MaSanBay
        };
        $http.put('http://localhost:3000/api/admin/sanbay/' + $scope.airports[index].MaSanBay, data).success(function(response){
            getAirPort();

        });
        if ($('#sidebar > ul').is(":visible") != true) {
            $('#main-content').css({
                'margin-left': '210px'
            });
            $('#sidebar > ul').show();
            $('#sidebar').css({
                'margin-left': '0'
            });
            $("#container").removeClass("sidebar-closed");
        }
    };

    $scope.AddAirPort = function () {

        var data = {
            Ten:  $scope.addtensanbay,
            MaSanBay: $scope.addmasanbay
        };
        $http.post('http://localhost:3000/api/admin/sanbay/', data).success(function(response){
            getAirPort();
        });
        $scope.addtensanbay = null;
        $scope.addmasanbay = null;
    };

    $scope.DeleteAirPort = function (item) {
        $http.delete('http://localhost:3000/api/admin/sanbay/' + item.MaSanBay).success(function(response){
            getAirPort();
        });
    };
}]);